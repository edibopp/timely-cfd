pub mod fluid;
pub mod riemann;
pub mod schemes;
pub mod slope_limiter;

pub fn diffusion(timestep: f64, grid_spacing: f64, stencil: [&f64; 3]) -> f64 {
    let derivative = (stencil[0] - 2. * stencil[1] + stencil[2])
        / grid_spacing.powi(2);
    stencil[1] + derivative * timestep
}

pub fn advection_upwind(timestep: f64, grid_spacing: f64, stencil: [&f64; 3]) -> f64 {
    let derivative = (stencil[2] - stencil[1]) / grid_spacing;
    stencil[1] + derivative * timestep
}
