use nalgebra::Vector2;
use super::{HyperbolicEquation, SignalSpeed};

#[derive(Clone, Copy)]
pub struct Isothermal1DFluid {
    pub sound_speed: f64
}

impl HyperbolicEquation<Vector2<f64>> for Isothermal1DFluid {
    type Flux = Vector2<f64>;

    fn flux(&self, quantity: &Vector2<f64>) -> Vector2<f64> {
        let (density, momentum) = (quantity[0], quantity[1]);
        let density_flux = momentum;
        let momentum_flux = if density > 0.0 {
            momentum.powi(2) / density + density * self.sound_speed.powi(2)
        } else {
            0.0
        };
        Vector2::new(density_flux, momentum_flux)
    }
}

impl SignalSpeed<Vector2<f64>> for Isothermal1DFluid {
    type Speed = f64;

    fn left_speed(&self, quantity: &Vector2<f64>) -> f64 {
        self.speed(quantity) - self.sound_speed
    }

    fn right_speed(&self, quantity: &Vector2<f64>) -> f64 {
        self.speed(quantity) + self.sound_speed
    }
}

impl Isothermal1DFluid {
    fn speed(&self, quantity: &Vector2<f64>) -> f64 {
        if quantity[0] > 0.0 {
            quantity[1] / quantity[0]
        } else {
            0.0
        }
    }
}
