use alga::linear::FiniteDimVectorSpace;

pub fn multi_component<V, F>(limiter: F, stencil: [&V; 3]) -> V
    where V: FiniteDimVectorSpace<Field=f64>,
          F: Fn([&f64; 3]) -> f64
{
    (0..V::dimension())
        .map(|i| V::canonical_basis_element(i) * limiter([&stencil[0][i], &stencil[1][i], &stencil[2][i]]))
        .fold(V::zero(), |a: V, b| a + b)
}

pub fn minbee(stencil: [&f64; 3]) -> f64 {
    let left_jump  = *stencil[1] - *stencil[0];
    let right_jump = *stencil[2] - *stencil[1];
    let max = |a, b| if a > b { a } else { b };
    let min = |a, b| if a < b { a } else { b };
    if right_jump > 0. {
        max(0., min(left_jump, right_jump))
    } else {
        min(0., max(left_jump, right_jump))
    }
}

pub fn superbee(stencil: [&f64; 3]) -> f64 {
    let left_jump  = *stencil[1] - *stencil[0];
    let right_jump = *stencil[2] - *stencil[1];
    let max = |a, b| if a > b { a } else { b };
    let min = |a, b| if a < b { a } else { b };
    if right_jump > 0. {
        max(0., max(min(2. * left_jump, right_jump), min(left_jump, 2. * right_jump)))
    } else {
        min(0., min(max(2. * left_jump, right_jump), max(left_jump, 2. * right_jump)))
    }
}
