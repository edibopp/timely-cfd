use std::io::Write;
use std::path::Path;
use std::error::Error;
use std::fs::File;
use serde::Serialize;
use rmp::Serializer;
use geometry::Grid;

pub fn write_msgpack<P: AsRef<Path>, T: Serialize>(name: P, grid: &Grid<T>) -> Result<(), Box<Error>> {
    let mut file = File::create(name)?;
    let mut buffer = Vec::new();
    // TODO: change to Serializer::new_named with new version of rmp_serde
    grid.serialize(&mut Serializer::new(&mut buffer))?;
    file.write_all(&buffer)?;
    Ok(())
}

pub fn filename(index: usize, timestep: u64) -> String {
    format!("output/step-{:04}.{}.msgpack", timestep, index)
}
