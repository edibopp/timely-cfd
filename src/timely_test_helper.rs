use std::cmp::Ordering;
use timely;
use timely::Data;
use timely::dataflow::stream::Stream;
use timely::dataflow::operators::{Capture, Map};
use timely::dataflow::operators::capture::Extract;
use timely::dataflow::scopes::{Child, Root};
use timely_communication::Allocator;

pub fn capture<D, F>(number_of_threads: usize, computation: F) -> Vec<Vec<D>>
    where F: for<'a> Fn(&mut Child<'a, Root<Allocator>, u64>) -> Stream<Child<'a, Root<Allocator>, u64>, D> + Send + Sync + 'static,
          D: Data + Send
{
    use std::sync::{Arc, Mutex};

    // get send and recv endpoints, wrap send to share
    let (send, recv) = ::std::sync::mpsc::channel();
    let send = Arc::new(Mutex::new(send));

    // execute a timely dataflow using three worker threads.
    timely::execute(timely::Configuration::Process(number_of_threads), move |worker| {
        let send = send.lock().unwrap().clone();
        worker.dataflow::<u64,_,_>(|scope| {
            let index = scope.index();
            computation(scope)
                .map(move |data| OrderByIndex { index, data })
                .capture_into(send);
        });
    }).unwrap();
    recv.extract().into_iter()
        .map(|(_time, ordered)| ordered.into_iter().map(|o| o.data).collect())
        .collect()
}

#[derive(Clone, Abomonation)]
struct OrderByIndex<T> {
    index: usize,
    data: T
}

impl<T> PartialEq<OrderByIndex<T>> for OrderByIndex<T> {
    fn eq(&self, other: &OrderByIndex<T>) -> bool {
        self.index.eq(&other.index)
    }
}

impl<T> Eq for OrderByIndex<T> {}

impl<T> PartialOrd<OrderByIndex<T>> for OrderByIndex<T> {
    fn partial_cmp(&self, other: &OrderByIndex<T>) -> Option<Ordering> {
        self.index.partial_cmp(&other.index)
    }
}

impl<T> Ord for OrderByIndex<T> {
    fn cmp(&self, other: &OrderByIndex<T>) -> Ordering {
        self.index.cmp(&other.index)
    }
}
