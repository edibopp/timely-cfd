#! /usr/bin/env nix-shell
#! nix-shell env/analysis.nix -i python

from itertools import count, dropwhile
import matplotlib.pyplot as plt
import numpy
import msgpack
import sys
from os.path import isfile

def main():
    timestep = int(sys.argv[1])
    plot_data(timestep, load_data(timestep))

def load_data(timestep):
    return [
        item
        for process in range(number_of_workers(timestep))
        for item in load_single_data(timestep, process)
    ]

def load_single_data(timestep, process):
    with open(output_file(timestep, process), 'rb') as binary:
        return msgpack.unpackb(binary.read())[0]

def number_of_workers(timestep):
    def exists(process):
        return isfile(output_file(timestep, process))
    return next(dropwhile(exists, count()))

def output_file(timestep, process):
    return 'output/step-{:04}.{}.msgpack'.format(timestep, process)

def plot_data(timestep, data):
    plt.figure(figsize=[12, 8])
    plt.plot(numpy.linspace(0, 1, len(data)), data)
    plt.xlim([0, 1])
    plt.ylim([-0.5, 1.5])
    plt.grid(True)
    make_sure_path_exists('plots/')
    plt.savefig('plots/step-{:04}.png'.format(timestep))
    plt.show()

def make_sure_path_exists(path):
    import os
    import errno
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

if __name__ == '__main__':
    main()
