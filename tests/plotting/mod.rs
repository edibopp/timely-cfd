use std::ops::Range;
use std::iter::once;

pub fn render_mismatch<F, I>(values: I, solution: F) -> String
    where F: Fn(f64) -> f64,
          I: Iterator<Item=(f64, f64)>
{
    let dimensions = [80, 24];
    let ranges = [0. .. 1., 0. .. 1.2];
    let (ok_values, wrong_values): (Vec<_>, _) = values.partition(|&(x, y)| solution(x) == y);
    let corrections = wrong_values.iter().map(|&(x, _)| (x, solution(x)));
    let points = normalize_all(ranges.clone(), dimensions, ok_values).map(|c| (c, '+'))
        .chain(normalize_all(ranges.clone(), dimensions, corrections).map(|c| (c, 'o')))
        .chain(normalize_all(ranges, dimensions, wrong_values.iter().cloned()).map(|c| (c, 'x')));
    plot_points(points, dimensions)
}

fn normalize_all<'a, I>(ranges: [Range<f64>; 2], dimensions: [usize; 2], values: I)
    -> Box<Iterator<Item=[usize; 2]> + 'a>
    where I: IntoIterator<Item=(f64, f64)> + 'a
{
    Box::new(
        values.into_iter()
            .filter_map(move |(x, y)| normalize_2d(ranges.clone(), [x, y], dimensions))
    )
}

fn plot_points<I>(points: I, dimensions: [usize; 2]) -> String
    where I: Iterator<Item=([usize; 2], char)>
{
    use std::collections::HashMap;
    let set: HashMap<_, _> = points.collect();
    (0 .. dimensions[1])
        .rev()
        .flat_map(|y| (0 .. dimensions[0])
            .map(move |x| [x, y])
            .map(|coord| set.get(&coord).map(|&x| x).unwrap_or(' '))
            .chain(once('\n')))
        .fold("".to_string(), |mut acc, s| { acc.push(s); acc })
}

#[test]
fn plots_points() {
    use std::iter::once;
    assert_eq!(
        plot_points(once(([1, 2], 'x')), [3, 3]),
        " x \n   \n   \n"
    );
}


fn normalize_2d(ranges: [Range<f64>; 2], value: [f64; 2],
                 dimensions: [usize; 2]) -> Option<[usize; 2]>
{
    let normalize = |i: usize|
        normalize(ranges[i].clone(), dimensions[i], value[i]);
    match (normalize(0), normalize(1)) {
        (Some(x), Some(y)) => Some([x, y]),
        _ => None
    }
}

#[test]
fn normalizes_point() {
    assert_eq!(
        normalize_2d([0. .. 1., 0. .. 1.], [0.5, 0.5], [5, 3]),
        Some([2, 1])
    );
}

#[test]
fn drops_points_outside_ranges() {
    assert_eq!(
        normalize_2d([0. .. 1., 0. .. 1.], [0.5, 1.5], [5, 3]),
        None
    );
}

fn normalize(range: Range<f64>, width: usize, value: f64) -> Option<usize> {
    if value <= range.end && value >= range.start {
        let ratio = (value - range.start) / (range.end - range.start);
        let target = (ratio * width as f64).floor() as usize;
        Some(if target >= width { width - 1 } else { target })
    } else {
        None
    }
}

#[test]
fn normalizes_value_in_range() {
    assert_eq!(normalize(0.0 .. 1.0, 100, 0.5), Some(50));
}

#[test]
fn does_not_normalize_values_outside_range() {
    assert_eq!(normalize(0.0 .. 1.0, 100, -0.2), None);
}

#[test]
fn lower_boundary_is_normalized_correctly() {
    assert_eq!(normalize(3.1 .. 4.2, 10, 3.1), Some(0))
}

#[test]
fn upper_boundary_is_normalized_correctly() {
    assert_eq!(normalize(-3.4 .. -2.2, 9, -2.2), Some(8))
}
