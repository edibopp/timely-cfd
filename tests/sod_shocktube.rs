extern crate timely;
extern crate timely_cfd;
extern crate nalgebra;

use timely::dataflow::operators::*;
use timely_cfd::dataflow::{scan, boundary_stepper_1d};
use timely_cfd::geometry::{Boundaries, Grid};
use timely_cfd::physics::riemann::{RiemannProblem, Primitive};
use timely_cfd::physics::fluid::Adiabatic1DFluid;
use nalgebra::Vector3;

mod plotting;

#[test]
fn classical_sod_shocktube() {
    test_consistency(RiemannProblem {
        left: Primitive { density: 1.0, pressure: 1.0, velocity: 0.0 },
        right: Primitive { density: 0.125, pressure: 0.1, velocity: 0.0 },
        fluid: Adiabatic1DFluid { adiabatic_index: 7. / 5. }
    });
}

#[test]
fn mirrored_sod_shocktube() {
    test_consistency(RiemannProblem {
        left: Primitive { density: 0.125, pressure: 0.1, velocity: 0.0 },
        right: Primitive { density: 1.0, pressure: 1.0, velocity: 0.0 },
        fluid: Adiabatic1DFluid { adiabatic_index: 7. / 5. }
    });
}

#[test]
fn problem_123() {
    test_consistency(RiemannProblem {
        left: Primitive { density: 1.0, pressure: 0.4, velocity: -2.0 },
        right: Primitive { density: 1.0, pressure: 0.4, velocity: 2.0 },
        fluid: Adiabatic1DFluid { adiabatic_index: 7. / 5. }
    });
}

fn test_consistency(shocktube: RiemannProblem) {
    timely::example(move |scope| {
        let number_of_cells = 400;
        let initial = Grid::from_fn(
            number_of_cells, scope.index(), scope.peers(),
            move |location| solution(shocktube, location, 0.0)
        );
        let state = scan(scope, (0, initial), move |state| {
            boundary_stepper_1d(state, move |grid, boundaries| step(shocktube.fluid, grid, boundaries))
                .filter(|&(timestep, _)| timestep <= 100)
        });
        state.inspect(move |&(timestep, ref grid)| {
            let time = timestep as f64 * 0.2 / number_of_cells as f64;
            if timestep == 100 {
                let valid_cells = grid.cells_with_coordinates().filter(|&(x, _)| x < 0.75 && x > 0.25);
                compare_solution(
                    valid_cells.map(|(x, v)| (x, v[0])),
                    move |location| solution(shocktube, location, time)[0]
                );
            }
        });
    });
}

fn solution(shocktube: RiemannProblem, location: f64, time: f64) -> Vector3<f64> {
    let primitives = shocktube.solve(time, location - 0.5);
    shocktube.fluid.from_primitive(primitives[0], primitives[1], primitives[2])
}

fn step(fluid: Adiabatic1DFluid, grid: Grid<Vector3<f64>>, boundaries: Boundaries<Vector3<f64>>)
    -> Grid<Vector3<f64>>
{
    use timely_cfd::physics::schemes::godunov;
    use timely_cfd::physics::riemann::hll;

    let timestep = 0.2 * grid.spacing();
    godunov(timestep, grid, [boundaries.left, boundaries.right], |left, right| hll(fluid, *left, *right))
}

fn compare_solution<F, I>(values: I, solution: F)
    where F: Fn(f64) -> f64,
          I: Iterator<Item=(f64, f64)>
{
    let values = values.collect::<Vec<_>>();
    let matches = |&(x, y): &(f64, f64)| (solution(x) - y).abs() < 0.15;
    if !values.iter().all(&matches) {
        panic!(
            "Mismatch:\n".to_string()
            + &plotting::render_mismatch(values.iter().cloned(), &solution)
            + &values.iter()
                .filter(|&p| !matches(p))
                .fold("".to_string(), |acc, &(x, y)| acc + &format!("{} {} {}\n", x, y, solution(x)))
        );
    }
}
