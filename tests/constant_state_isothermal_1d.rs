extern crate timely_cfd;
extern crate nalgebra;
extern crate timely;

use nalgebra::Vector2;
use timely::Data;
use timely::dataflow::operators::*;
use timely::dataflow::stream::Stream;
use timely::dataflow::scopes::{Child, ScopeParent};
use timely_cfd::grid::Grid;
use timely_cfd::grid_stream::DistributedGrid;
use timely_cfd::physics::schemes::alt::godunov;
use timely_cfd::physics::riemann::hll;
use timely_cfd::physics::fluid::Isothermal1DFluid;

#[test]
fn constant_state_is_preserved_on_local_grid() {
    let number_of_cells = 10;
    let initial = Grid::from_fns(number_of_cells,
        |i| i as f64 / number_of_cells as f64,
        |_| Vector2::new(1., 0.)
    );

    let mut state = initial.clone();

    for _ in 0..10 {
        let boundaries = state.periodic_boundaries();
        let timestep = 0.05;
        state = godunov(timestep, state, boundaries, solver);
        for (_, value) in state.clone() {
            assert_eq!(value[0], 1.0);
        }
    }
}

#[test]
fn constant_state_is_preserved_on_distributed_grid() {
    use timely_cfd::timely_test_helper::capture;
    let simulation = capture(1, |scope| {
        let number_of_cells = 10;
        let initial = DistributedGrid::from_fns(scope, number_of_cells,
            |i| i as f64 / number_of_cells as f64,
            |_| Vector2::new(1., 0.)
        );
        scan(scope, initial.to_stream(), |state| {
            let timestep = 0.05;
            let grid = DistributedGrid::from_stream(state.clone());
            let boundaries = grid.periodic_boundaries();
            godunov(timestep, grid, boundaries, solver)
                .to_stream()
        })
    });
    for frame in simulation {
        for value in &frame[0].grid.values {
            assert_eq!(value[0], 1.);
        }
    }
}

fn solver(left: &Vector2<f64>, right: &Vector2<f64>) -> Vector2<f64> {
    hll(Isothermal1DFluid { sound_speed: 1.0 }, *left, *right)
}

fn scan<'a, T, S, G>(scope: &mut Child<'a, G, u64>, initial: Stream<Child<'a, G, u64>, T>, stepper: S) -> Stream<Child<'a, G, u64>, T>
    where G: ScopeParent,
          S: for<'b> FnOnce(&Stream<Child<'b, G, u64>, T>) -> Stream<Child<'b, G, u64>, T> + 'static,
          T: Data
{
    let number_of_cycles = 10;
    let (helper, cycle) = scope.loop_variable(number_of_cycles, 1);
    let state = initial.concat(&cycle);
    stepper(&state).connect_loop(helper);
    state
}
