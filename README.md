# Examples

There are a bunch of examples in the `examples/` directory. Run them using
`cargo`:

```sh
cargo run --example=isothermal_sod_shocktube --release -- -w 2
```

The `--release` flag significantly speeds up computations, so it is generally
recommended.

You can pass the usual [timely dataflow][timely] arguments to the examples to
run it with multiple threads or processes on seperate nodes.

[timely]: https://github.com/frankmcsherry/timely-dataflow


# Environment setup

A Nix expression is available that describes the environment required to compile
and run the code on your machine. It requires nothing special beyond a nightly
Rust compiler.

It has been tested on the NixOS unstable branch (2017-08-01) and uses the
[Mozilla nixpkgs overlay][mozilla-nixpkgs].

[mozilla-nixpkgs]: https://github.com/mozilla/nixpkgs-mozilla
