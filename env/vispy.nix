with import <nixpkgs> {};
with pkgs.python27Packages;

buildPythonPackage rec {
  name = "vispy-master";

  src = pkgs.fetchgit {
    url = "https://github.com/vispy/vispy";
    sha256 = "1zm0gglvzlhliicwv5rm9nv34f1ql2i5vns8ybqr78r4bcz0f6dp";
    rev = "38001b70d54a5519eb296d44a196c081fcbd7fad";
  };

  propagatedBuildInputs = with pkgs; [
    numpy
    pyqt4
    pyopengl
    fontconfig
    freetype
    mesa_noglu
  ];

  postPatch = ''
    substituteInPlace vispy/ext/fontconfig.py \
      --replace "util.find_library('fontconfig')" \
      "'${pkgs.fontconfig.lib}/lib/libfontconfig.so.1'"

    substituteInPlace vispy/gloo/gl/gl2.py \
      --replace "ctypes.util.find_library('GL')" \
      "'${pkgs.mesa_noglu}/lib/libGL.so'"

    substituteInPlace vispy/ext/_bundled/freetype.py \
      --replace "util.find_library('freetype')" \
      "'${pkgs.freetype}/lib/libfreetype.so'"
  '';

  meta = {
    homepage = "http://vispy.org/";
    description = "Interactive scientific data visualization";
    license = licenses.bsd3;
    maintainers = [];
  };
}
